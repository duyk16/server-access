import nock from 'nock';

import { findServer, isServerOnline } from '../src';

const SERVERS = [
  {
    url: 'https://does-not-work.perfume.new',
    priority: 1,
  },
  {
    url: 'https://gitlab.com',
    priority: 4,
  },
  {
    url: 'http://app.scnt.me',
    priority: 3,
  },
  {
    url: 'https://offline.scentronix.com',
    priority: 2,
  },
];

describe('isServerOnline', () => {
  it('should return true for an online server (status code 200)', async () => {
    nock('https://does-not-work.perfume.new').get('/').once().reply(200);

    const result = await isServerOnline(SERVERS[0]);
    expect(result).toBe(true);
  });

  it('should return false for an offline server (status code 404)', async () => {
    nock('https://does-not-work.perfume.new').get('/').once().reply(404);

    const result = await isServerOnline(SERVERS[0]);
    expect(result).toBe(false);
  });

  it('should return false on fetch error', async () => {
    nock('https://does-not-work.perfume.new')
      .get('/')
      .once()
      .replyWithError('Fetch error');

    const result = await isServerOnline(SERVERS[0]);
    expect(result).toBe(false);
  });

  it('should return false when the server request times out 5s', async () => {
    nock('https://does-not-work.perfume.new')
      .get('/')
      .delay(6000)
      .once()
      .reply(200);

    const result = await isServerOnline(SERVERS[0]);
    expect(result).toBe(false);
  });
});

describe('findServer', () => {
  it('should find the online server with the lowest priority', async () => {
    nock('https://does-not-work.perfume.new').get('/').once().reply(404);
    nock('https://gitlab.com').get('/').once().reply(200);
    nock('http://app.scnt.me').get('/').once().reply(400);
    nock('https://offline.scentronix.com').get('/').once().reply(200);

    const result = await findServer(SERVERS);
    expect(result.url).toBe('https://offline.scentronix.com');
  });

  it('should reject with an error if no servers are online', async () => {
    nock('https://does-not-work.perfume.new').get('/').once().reply(400);
    nock('https://gitlab.com').get('/').once().reply(400);
    nock('http://app.scnt.me').get('/').once().reply(400);
    nock('https://offline.scentronix.com').get('/').once().reply(400);

    await expect(findServer(SERVERS)).rejects.toThrow('no servers are online');
  });
  it('should reject with an error if no servers are provided', async () => {
    await expect(findServer([])).rejects.toThrow('no servers provided');
  });
});
