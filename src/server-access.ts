import Axios from 'axios';

interface Server {
  url: string;
  priority: number;
}

const DEFAULT_REQUEST_TIMEOUT = 5000; // 5s

// Function to check if a server is online
export async function isServerOnline(server: Server): Promise<boolean> {
  try {
    const response = await Axios.get(server.url, {
      timeout: DEFAULT_REQUEST_TIMEOUT,
    });
    return response.status >= 200 && response.status < 300;
  } catch (error) {
    return false; // Server is considered offline on error
  }
}

// Function to assess webservers and return the one with the lowest priority
export async function findServer(servers: Server[]): Promise<Server> {
  if (!Array.isArray(servers) || servers.length === 0) {
    throw new Error('no servers provided');
  }

  const fetchedOnlineServers = await Promise.all(
    servers.map(async (server) => {
      const isOnline = await isServerOnline(server);
      return isOnline ? server : null;
    }),
  );

  const onlineServers = fetchedOnlineServers.filter(
    (server) => server !== null,
  ) as unknown as Server[];

  if (onlineServers.length === 0) {
    throw new Error('no servers are online');
  }

  let lowestPriorityServer = onlineServers[0];

  for (const server of onlineServers) {
    if (server.priority < lowestPriorityServer.priority) {
      lowestPriorityServer = server;
    }
  }

  return lowestPriorityServer;
}
