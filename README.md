## Usage

### Import the Module

```javascript
import { findServer } from './src/serverAssessment';
```

### Find the Online Server with Lowest Priority

```javascript
const SERVERS = [
  {
    url: 'https://does-not-work.perfume.new',
    priority: 1,
  },
  {
    url: 'https://gitlab.com',
    priority: 4,
  },
  {
    url: 'http://app.scnt.me',
    priority: 3,
  },
  {
    url: 'https://offline.scentronix.com',
    priority: 2,
  },
];

findServer(SERVERS)
  .then((onlineServer) => {
    console.log('The online server with the lowest priority is:', onlineServer.url);
  })
  .catch((error) => {
    console.error('Error:', error.message);
  });
```

### Running Tests

To run unit tests using Jest, use the following command:

```bash
yarn test
```

## Testing

The project includes unit tests to ensure the functionality of the server assessment module. You can find the test cases in the `tests` directory.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
```